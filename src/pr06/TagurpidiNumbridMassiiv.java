package pr06;

public class TagurpidiNumbridMassiiv {

	public static void main(String[] args) {

		// Massiivi defineerimine
		System.out.println("Sisesta suvalises j�rjekorras k�mme arvu ");
		int[] arvud = new int[10];

		// Arvude k�simine ehk massiivi t�itmine
		for (int i = 0; i < arvud.length; i++) {
			System.out.print("Sisesta arv: ");
			arvud[i] = TextIO.getlnInt();
		}

		// Sisestatud arvude j�rjekorra vastupidiseks muutumine
		for (int j = arvud.length - 1; j >= 0; j--) {
			System.out.printf("%4d", arvud[j]);
		}
	}
}
