package pr14;

public class Joon {

	Punkt alguspunkt, l6pppunkt;

	public Joon(Punkt alguspunkt, Punkt l6pppunkt) {
		this.alguspunkt = alguspunkt;
		this.l6pppunkt = l6pppunkt;
	}

	@Override
	public String toString() {
		return "Joon(" + alguspunkt + "," + l6pppunkt + ")";
	}

	public double pikkus() {
		double a = l6pppunkt.getX() - alguspunkt.getY();
		double b = alguspunkt.getX() - l6pppunkt.getY();
		double p = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
		return p;
	}
	


}
